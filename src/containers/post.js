import React from 'react';
import { connect } from 'react-redux';
import { postText } from '../actions';
import Card from 'react-bootstrap/Card'
class Post extends React.Component {
    state = {
        text: ''
    };

    handleInputChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    handleSubmit = e => {
        e.preventDefault();
        if (this.state.text.trim()) {
            this.props.onPost(this.state);
            this.handleReset();
        }
    };

    handleReset = () => {
        document.getElementById('response').innerHTML = '';
        this.setState({
            text: '',
        });
    };

    render() {
        const {text}=this.props.text;

        return (
            <div>
                <Card>
                    <Card.Body>
                        <div>
                            <form onSubmit={ this.handleSubmit }>
                                <div className="form-group">
                                    <input
                                        type="text"
                                        placeholder="Text"
                                        className="form-control"
                                        name="text"
                                        onChange={ this.handleInputChange }
                                        value={ this.state.text }
                                    />
                                </div>
                                <div className="form-group">
                                </div>
                                <div className="form-group">
                                    <button type="submit" className="btn btn-primary">POST</button>
                                    <button type="button" className="btn btn-warning" onClick={ this.handleReset }>
                                        RESET
                                    </button>
                                </div>
                            </form>
                        </div>
                    </Card.Body>
                </Card>
                <Card style={{ marginTop: '1rem' }}>
                    <Card.Body>
                        <Card.Title id='response'>Response</Card.Title>
                        <Card.Text>
                            {text}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}

const mapStateToProps = text => {
    return {
        text
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onPost: text => {
            postText(text).then(function (result) {
                dispatch(result);
            });
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Post);
