import axios from 'axios';
import { apiURL } from '../utils/apiUtils';
import { POST_TEXT } from './actionTypes';

export const postText = async (text) => {
    const url = apiURL+'/text';
    const data = await axios.post(url,{
        headers: {
            'Content-Type':  'application/json'
        },
        text,
    }).then(function (response) {
        return response.data.text
    }).catch(function (err) {
        return err
    })
    return {
        type: POST_TEXT,
        payload: data
    }
};
