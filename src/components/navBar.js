import React from 'react';
import Navbar from 'react-bootstrap/Navbar'

export default function NavBar(){
    return (
        <Navbar bg="primary" variant="dark">
            <Navbar.Brand href="#home">My App Web Portal</Navbar.Brand>
        </Navbar>

    );
};
