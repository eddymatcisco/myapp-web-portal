import React from 'react';
import './App.css';
import Post from "./containers/post";
import NavBar from "./components/navBar";

function App() {
  return (
      <div>
          <NavBar/>
          <div className="App">
              <Post/>
          </div>
      </div>

  );
}

export default App;
