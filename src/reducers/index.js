import { combineReducers } from 'redux';
import text from './postTextReducer';

export default combineReducers({
    text: text
});
