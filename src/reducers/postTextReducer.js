import { POST_TEXT } from '../actions/actionTypes';

export default function postTextReducer(state = [], action) {
    switch (action.type) {
        case POST_TEXT:
            return action.payload.text;
        default:
            return state;
    }
}
